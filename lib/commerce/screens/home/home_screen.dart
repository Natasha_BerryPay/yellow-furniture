import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yellow_furniture/commerce/constants.dart';
import 'package:yellow_furniture/commerce/screens/home/components/body.dart';
import 'package:yellow_furniture/commerce/screens/home/components/search_page.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Body(),
    );
  }

  AppBar buildAppBar(context) {
    return AppBar(
      backgroundColor: Colors.white,
      // leading: IconButton(
      //   icon: SvgPicture.asset("assets/icons/back.svg"),
      //   onPressed: () {},
      // ),
      actions: <Widget>[
        IconButton(
          icon: SvgPicture.asset(
            "assets/icons/search.svg",
            // By default our  icon color is white
            color: kTextColor,
          ),
          onPressed: () {
            Navigator.push(context,
                CupertinoPageRoute(builder: (contect) => SearchPage()));
          },
        ),
        IconButton(
          icon: SvgPicture.asset(
            "assets/icons/cart.svg",
            // By default our  icon color is white
            color: kTextColor,
          ),
          onPressed: () {},
        ),
        SizedBox(width: kDefaultPaddin / 2)
      ],
    );
  }

  // Widget expandSearch() {
  //   print("inside Search");
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Text("Search"),
  //       actions: [],
  //     ),
  //     body: Column(
  //       children: <Widget>[
  //         buildSearch(),
  //         Expanded(
  //           child: ListView.builder(
  //             itemCount: allProducts.length,
  //             itemBuilder: (context, index) {
  //               final prod = products[index];

  //               return buildProduct(prod);
  //             },
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
