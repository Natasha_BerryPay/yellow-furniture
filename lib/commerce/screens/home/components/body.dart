import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yellow_furniture/commerce/constants.dart';
import 'package:yellow_furniture/commerce/models/Product.dart';
import 'package:yellow_furniture/commerce/screens/details/details_screen.dart';

import 'categorries.dart';
import 'item_card.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: kBackgroundColor),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(
                kDefaultPaddin), //symmetric(horizontal: kDefaultPaddin),
            child: Text(
              "Furniture",
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(fontWeight: FontWeight.bold),
            ),
          ),
          Categories(),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
              child: GridView.builder(
                  itemCount: allProducts.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: kDefaultPaddin,
                    crossAxisSpacing: kDefaultPaddin,
                    childAspectRatio: 0.75,
                  ),
                  itemBuilder: (context, index) => ItemCard(
                        product: allProducts[index],
                        press: () => Navigator.push(
                            context,
                            CupertinoPageRoute(
                              builder: (context) => DetailsScreen(
                                product: allProducts[index],
                              ),
                            )),
                      )),
            ),
          ),
        ],
      ),
    );
  }
}
