import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yellow_furniture/commerce/constants.dart';
import 'package:yellow_furniture/commerce/models/Product.dart';
import 'package:yellow_furniture/commerce/screens/details/details_screen.dart';
import 'package:yellow_furniture/commerce/screens/home/components/search_widget.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<Product> products;
  String query = '';
  final Function press;

  _SearchPageState({this.press, this.products});

  // _SearchPageState(this.press);

  @override
  void initState() {
    super.initState();

    products = allProducts;
  }

  @override
  Widget build(BuildContext context) {
    var prod;

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            icon: SvgPicture.asset(
              'assets/icons/back.svg',
              color: kTextColor,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          title: Text("Search", style: TextStyle(color: kTextColor))),
      body: Column(
        children: <Widget>[
          buildSearch(),
          Expanded(
            child: ListView.builder(
              itemCount: products.length,
              itemBuilder: (context, index) {
                prod = products[index];

                return buildProduct(prod);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'What to find?',
        onChanged: searchProducts,
      );

  Widget buildProduct(Product prod) => ListTile(
        leading: Image.asset(
          prod.image,
          fit: BoxFit.cover,
          width: 50,
          height: 50,
        ),
        title: Text(prod.title),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("${prod.colorCode}"),
            Text(
              "RM ${prod.price}",
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
        isThreeLine: true,
        onTap: () {
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (context) => DetailsScreen(product: prod),
            ),
          );
        },
        trailing: Icon(Icons.keyboard_arrow_right),
      );

  void searchProducts(String query) {
    final products = allProducts.where((product) {
      final titleLower = product.title.toLowerCase();
      // final priceLower = products.price;
      final codeLower = product.colorCode.toLowerCase();
      final searchLower = query.toLowerCase();

      return titleLower.contains(searchLower) ||
          codeLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.products = products;
    });
  }
}
