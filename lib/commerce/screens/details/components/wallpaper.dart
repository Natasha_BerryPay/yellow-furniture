import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:liquid_swipe/liquid_swipe.dart';
import 'package:yellow_furniture/commerce/models/Product.dart';

class WallpaperScreen extends StatelessWidget {
  final Product product;
  // final Body wallpaper;

  const WallpaperScreen({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pages = [
      Container(
        color: Colors.blue,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(product.image),
          ],
        ),
      ),
      Container(
        color: Colors.yellow,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(product.image),
          ],
        ),
      ),
      Container(
        color: Colors.pink,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(product.image),
          ],
        ),
      ),
      Container(
        color: Colors.green,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(product.image),
          ],
        ),
      ),
    ];

    return Scaffold(
      body: LiquidSwipe(pages: pages),
    );
  }
}
