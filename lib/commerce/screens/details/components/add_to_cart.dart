import 'dart:async';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:yellow_furniture/commerce/screens/details/components/place_order.dart';
import 'package:yellow_furniture/transaction.dart/transfer_bank_model.dart';
import 'package:yellow_furniture/commerce/models/Product.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import '../../../constants.dart';

class AddToCart extends StatefulWidget {
  const AddToCart({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  _AddToCartState createState() => _AddToCartState();
}

class _AddToCartState extends State<AddToCart> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  String txnRef;
  // On urlChanged stream
  StreamSubscription<String> _onUrlChanged;
  // On urlChanged stream
  StreamSubscription<WebViewStateChanged> _onStateChanged;

  @override
  void initState() {
    super.initState();
    flutterWebViewPlugin.close();

    // Add a listener to on url changed
    _onUrlChanged = flutterWebViewPlugin.onUrlChanged.listen((String url) {
      if (mounted &&
          url == "https://berrypay.biz/api/purchaseresponseindirect") {
        setState(() {
          // _renderFpxReceipt();
        });
      }
    });

    _onStateChanged =
        flutterWebViewPlugin.onStateChanged.listen((WebViewStateChanged state) {
      if (mounted &&
          state.url == "https://berrypay.biz/api/purchaseresponseindirect") {
        setState(() {
          //print('============================= onStateChanged: ${state.type} ${state.url}');
        });
      }
    });
  }

  @override
  void dispose() {
    _onUrlChanged.cancel();
    _onStateChanged.cancel();
    flutterWebViewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: kDefaultPaddin),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: kDefaultPaddin),
            height: 50,
            width: 58,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18),
              border: Border.all(
                color: kTextColor,
              ),
            ),
            child: IconButton(
              icon: SvgPicture.asset(
                "assets/icons/add_to_cart.svg",
                color: kTextColor,
              ),
              onPressed: () {},
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 50,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18)),
                color: kTextColor,
                onPressed: () {
                  // _openFpx();
                  Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (contect) =>
                              PlaceOrder(product: widget.product)));
                },
                child: Text(
                  "Buy Now".toUpperCase(),
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<Null> _openFpx() async {
    var amount = widget.product.price;
    var rng = new Random();
    var randomNo = new List.generate(2, (_) => rng.nextInt(100));
    DateFormat dateFormat = DateFormat("yyyyMMddHHmmss");
    txnRef = "BP" + randomNo.join('') + dateFormat.format(DateTime.now());
    var senderName = "Natasha Syazana";
    var senderPhone = "0136673455";

    Navigator.push<dynamic>(
        context,
        CupertinoPageRoute<dynamic>(
          builder: (BuildContext context) => WebviewScaffold(
            url:
                "https://berrypay.biz/api/v1/fpx/app/payment/664057?txn_order_id=$txnRef&txn_amount=${amount.toString()}&txn_buyer_name=$senderName&txn_buyer_email=remittance@berrypay.com&txn_buyer_phone=$senderPhone&txn_product_name=MR%20ransfer%20Bank&txn_product_desc=Mobile%20Remit%20Transfer%20Bank",
            withJavascript: true,
            appBar: AppBar(),
            initialChild: Container(
              color: Colors.white,
              child: const Center(
                child: Text('Waiting.....'),
              ),
            ),
          ),
        ));
  }

  // _renderFpxReceipt() {
  //   String status = "1";
  //   TransferBankRequest data = TransferBankRequest(
  //       pinNumber,
  //       senderName,
  //       nameTextFieldController.text,
  //       accountTextFieldController.text,
  //       phoneTextFieldController.text,
  //       bankName,
  //       bankCode,
  //       sgdTextFieldController.text,
  //       "10",
  //       remarksTextFieldController.text,
  //       rate,
  //       countryOrigin,
  //       txnRef,
  //       6);
  //   Navigator.pushReplacement(
  //       context,
  //       MaterialPageRoute(
  //           builder: (context) => SuccessTxnFpxPage(
  //               status, data, idrTextFieldController.text, total, currency)));
  // }
}
