import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yellow_furniture/commerce/models/Product.dart';
import 'package:yellow_furniture/commerce/screens/home/components/search_page.dart';

import '../../../constants.dart';

class PlaceOrder extends StatelessWidget {
  final Product product;

  const PlaceOrder({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: buildAppBar(context),
      body: detailsOrder(context),
    );
  }

  AppBar buildAppBar(context) {
    return AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: const Text("Checkout", style: TextStyle(color: kTextColor)),
        leading: IconButton(
          icon: SvgPicture.asset(
            'assets/icons/back.svg',
            color: kTextColor,
          ),
          onPressed: () => Navigator.pop(context),
        ));
  }

  Widget detailsOrder(context) {
    return SingleChildScrollView(
      child: Container(
          // decoration: BoxDecoration(color: kBackgroundColor),
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Shipping to",
                    style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.w800)),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.white),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Text("Checked"),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Home",
                                    style: TextStyle(
                                        fontSize: 1,
                                        fontWeight: FontWeight.w700)),
                                Text(
                                    "No. 5, Jalan Arked 1/4,\nTaman Arked, 43800 Dengkil.")
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Icon(Icons.edit),
                              ],
                            ),
                          ),
                        ],
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.white),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("Checked"),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Office",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700)),
                                Text(
                                    "No. 5, Jalan Arked 1/4,\nTaman Arked, 43800 Dengkil.")
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(Icons.edit),
                              ],
                            ),
                          ),
                        ],
                      )),
                ),
                SizedBox(height: 10),
                Text("Payment method",
                    style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.w800)),
                Row(
                  children: [
                    // Text(product.title),
                  ],
                ),
              ],
            ),
          )),
    );
  }
}
