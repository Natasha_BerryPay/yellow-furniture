import 'package:flutter/material.dart';

const kTextColor = Color(0xFF535353);
const kTextLightColor = Color(0xFFACACAC);
const kBackgroundColor = Color(0xFFf0f0f0);
const kItemColor = Color(0xFFfcfafa);

const kDefaultPaddin = 20.0;
