import 'package:flutter/material.dart';
import 'package:yellow_furniture/commerce/constants.dart';

class Product {
  final String image, title, description, colorCode;
  final int price, size, id;
  final Color color;
  const Product({
    this.id,
    this.title,
    this.price,
    this.size,
    this.colorCode,
    this.description,
    this.image,
    this.color,
  });

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json['id'],
        title: json['title'],
        price: json['price'],
        size: json['size'],
        colorCode: json['colorCode'],
        description: json['description'],
        image: json['image'],
        color: json['color'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'price': price,
        'size': size,
        'colorCode': colorCode,
        'description': description,
        'image': image,
        'color': color,
      };
}

// List<Product> products = [
final allProducts = <Product>[
  Product(
      id: 1,
      title: "Lafeez",
      price: 205,
      size: 12,
      colorCode: "#F5F5F5",
      description: dummyText,
      image: "assets/images/chair_yellow.png",
      color: kItemColor),
  Product(
      id: 2,
      title: "Ronde",
      price: 234,
      size: 8,
      colorCode: "#CBC123",
      description: dummyText,
      image: "assets/images/chair_1.png",
      color: kItemColor),
  Product(
      id: 3,
      title: "Gharit",
      price: 399,
      size: 10,
      colorCode: "#FD1523",
      description: dummyText,
      image: "assets/images/chair_2.png",
      color: kItemColor),
  Product(
      id: 4,
      title: "Fier",
      price: 98,
      size: 11,
      colorCode: "#DDF452",
      description: dummyText,
      image: "assets/images/chair_3.png",
      color: kItemColor),
  Product(
      id: 5,
      title: "Olugi",
      price: 589,
      size: 412,
      colorCode: "#A42511",
      description: dummyText,
      image: "assets/images/chair_4.png",
      color: kItemColor),
  Product(
    id: 6,
    title: "Relni",
    price: 176,
    size: 12,
    colorCode: "#B12F35",
    description: dummyText,
    image: "assets/images/demo_yellow.png",
    color: kItemColor,
  ),
  Product(
    id: 7,
    title: "Zerham",
    price: 206,
    size: 12,
    colorCode: "#CA263B",
    description: dummyText,
    image: "assets/images/demo_yellow_2.png",
    color: kItemColor,
  ),
  Product(
    id: 8,
    title: "Eleminos",
    price: 76,
    size: 12,
    colorCode: "#B12ADF",
    description: dummyText,
    image: "assets/images/armchair.png",
    color: kItemColor,
  ),
];

String dummyText = "Soft. Using 100% cotton.";
