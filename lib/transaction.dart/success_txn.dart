import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:yellow_furniture/transaction.dart/transfer_bank_model.dart';

class SuccessTxnFpxPage extends StatefulWidget {
  final String status;
  final TransferBankRequest data;
  final String idr;
  final double total;
  final String currency;
  SuccessTxnFpxPage(
      this.status, this.data, this.idr, this.total, this.currency);
  @override
  _SuccessTxnFpxPageState createState() => _SuccessTxnFpxPageState();
}

class _SuccessTxnFpxPageState extends State<SuccessTxnFpxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 50),
            Center(
              child: SizedBox(
                width: 150,
                height: 150,
                child: FlareActor(
                  widget.status == "1"
                      ? "assets/icons/Success.flr"
                      : "assets/icons/Error.flr",
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                  animation: widget.status == "1" ? "sucess" : "Error",
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Center(
                child: Text(
                    widget.status == "1"
                        ? "Your transfer money is under process."
                        : "Your transfer money was failed.",
                    style: TextStyle(fontSize: 17)),
              ),
            ),
            SizedBox(height: 16),
            Padding(
                padding: const EdgeInsets.only(
                    top: 8, left: 16, right: 16, bottom: 16),
                child: Container(
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.05),
                        blurRadius:
                            5.0, // has the effect of softening the shadow
                        spreadRadius:
                            1.0, // has the effect of extending the shadow
                        offset: Offset(
                          0.0, // horizontal, move right 10
                          3.0, // vertical, move down 10
                        ),
                      ),
                    ],
                  ),
                  child: Column(children: <Widget>[
                    Text("Transfer Details",
                        style: TextStyle(color: Colors.grey)),
                    Text("${widget.data.merchantRefId}",
                        style: TextStyle(color: Colors.black, fontSize: 17)),
                    SizedBox(height: 25.0),
                    Row(children: <Widget>[
                      Expanded(
                          child: Text("Receiver",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ))),
                      Text(widget.data.beneficiaryName,
                          style: TextStyle(fontSize: 14.0)),
                    ]),
                    SizedBox(height: 8.0),
                    Row(children: <Widget>[
                      Expanded(
                          child: Text("Bank Name",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ))),
                      Text(widget.data.bankName,
                          style: TextStyle(fontSize: 14.0)),
                    ]),
                    SizedBox(height: 8.0),
                    Row(children: <Widget>[
                      Expanded(
                          child: Text("Bank Account",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ))),
                      Text(widget.data.beneficiaryAccount,
                          style: TextStyle(fontSize: 14.0)),
                    ]),
                    SizedBox(height: 16.0),
                    // MySeparator(),
                    SizedBox(height: 16.0),
                    Row(children: <Widget>[
                      Expanded(
                          child: Text("Send",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ))),
                      Text("${widget.currency} ${widget.data.amount}",
                          style: TextStyle(fontSize: 14.0)),
                    ]),
                    SizedBox(height: 8.0),
                    Row(children: <Widget>[
                      Expanded(
                          child: Text("Receive",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ))),
                      Text("IDR ${widget.idr}",
                          style: TextStyle(fontSize: 14.0)),
                    ]),
                    SizedBox(height: 16.0),
                    // MySeparator(),
                    SizedBox(height: 16.0),
                    Row(children: <Widget>[
                      Expanded(
                          child: Text("Admin Fee",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15.0,
                              ))),
                      Text("+ ${widget.currency} 10",
                          style: TextStyle(fontSize: 14.0)),
                    ]),
                    SizedBox(height: 16.0),
                    // MySeparator(),
                    SizedBox(height: 16.0),
                    Container(
                      padding: EdgeInsets.only(bottom: 16.0),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(color: Colors.black)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Promotion",
                              style: TextStyle(
                                  color: Colors.grey, fontSize: 15.0)),
                          Text('No Promo Code',
                              style: TextStyle(color: Colors.grey)),
                        ],
                      ),
                    ),
                    SizedBox(height: 16.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Total pay",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15.0)),
                        Text(
                          '${widget.currency} ${widget.total}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                          ),
                        ),
                      ],
                    ),
                  ]),
                )),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: SizedBox(height: 48.0, child: _doneButton()),
            ),
          ],
        )),
      ),
    );
  }

  Widget _doneButton() {
    return FlatButton(
      onPressed: () {
        // Navigator.pushAndRemoveUntil(
        //     context,
        //     MaterialPageRoute(builder: (BuildContext context) => Tabs()),
        //     ModalRoute.withName('/'
        //     ));
      },
      color: Colors.red,
      shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(24.0),
          side: BorderSide(color: Colors.red)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Done",
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(width: 3),
          Icon(
            Icons.check,
            size: 17,
            color: Colors.white,
          )
        ],
      ),
    );
  }
}
