class TransferBankRequest {
  String confirmationPin;
  String senderName;
  String beneficiaryName;
  String beneficiaryAccount;
  String beneficiaryPhone;
  String bankName;
  String bankCode;
  String amount;
  String additionalFee;
  String notes;
  String exchangeRate;
  String countryOrigin;
  String merchantRefId;
  int type;

  TransferBankRequest(
      this.confirmationPin,
      this.senderName,
      this.beneficiaryName,
      this.beneficiaryAccount,
      this.beneficiaryPhone,
      this.bankName,
      this.bankCode,
      this.amount,
      this.additionalFee,
      this.notes,
      this.exchangeRate,
      this.countryOrigin,
      this.merchantRefId,
      this.type);
}

class TransferBankResponse {
  bool status;
  int statusCode;
  String message;
  TransactionBankData data;

  TransferBankResponse(this.status, this.statusCode, this.message, this.data);

  TransferBankResponse.fromJson(Map<String, dynamic> json)
      : status = json["status"],
        statusCode = json["statusCode"],
        message = json["message"],
        data = TransactionBankData.fromJson(json["data"]);
}

class TransactionBankData {
  int transactionId;
  String transactionRefId;

  TransactionBankData(this.transactionId, this.transactionRefId);

  TransactionBankData.fromJson(Map<String, dynamic> json)
      : transactionId = json["transactionId"],
        transactionRefId = json["transactionRefId"];
}
