import 'dart:ui';
import 'package:flutter/material.dart';

class Global {
  static const Color white = const Color(0xfff7f7f7);
  static const Color mediumYellow = const Color(0xFFffd078);
  static const List<Color> palette = [white, mediumYellow];
  static const double scale = 1;
  static const double radius = 88.0;
  static const double bottomPadding = 75.0;
}
