import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:yellow_furniture/core/viewmodels/home_model.dart';
import 'package:yellow_furniture/register/register_page.dart';
import 'package:yellow_furniture/ui/shared/globals.dart';
import 'package:provider/provider.dart';
import 'package:yellow_furniture/login/login_page.dart';

class AnimatedCircle extends AnimatedWidget {
  final Tween<double> tween;
  final Tween<double> horizontalTween;
  final Animation<double> animation;
  final Animation<double> horizontalAnimation;
  final double flip;
  final Color color;

  AnimatedCircle({
    Key key,
    @required this.animation,
    this.horizontalTween,
    this.horizontalAnimation,
    @required this.color,
    @required this.flip,
    @required this.tween,
  })  : assert(flip == 1 || flip == -1),
        super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<HomeModel>(context);

    return Column(
      children: [
        // Text('Hello'),
        // Image.asset("assets/images/chair_yellow.png"),
        Transform(
          alignment: FractionalOffset.centerLeft,
          transform: Matrix4.identity()
            ..scale(
              tween.evaluate(animation) * flip,
              tween.evaluate(animation),
            ),
          child: Transform(
            transform: Matrix4.identity()
              ..translate(
                horizontalTween != null
                    ? horizontalTween.evaluate(horizontalAnimation)
                    : 0.0,
              ),
            child: Container(
              width: Global.radius,
              height: Global.radius,
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(
                  Global.radius / 2.0 -
                      tween.evaluate(animation) / (Global.radius / 2.0),
                ),
              ),
              child: Icon(
                flip == 1
                    ? Icons.keyboard_arrow_right
                    : Icons.keyboard_arrow_left,
                color:
                    model.index % 2 == 0 ? Global.white : Global.mediumYellow,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with TickerProviderStateMixin {
  AnimationController animationController;
  Animation startAnimation;
  Animation endAnimation;
  Animation horizontalAnimation;
  PageController pageController;

  static const TextStyle blackStyle = TextStyle(
      fontSize: 40.0, color: Colors.black, fontFamily: "Product Sans");

  static const TextStyle whiteBoldStyle = TextStyle(
      fontSize: 70.0,
      color: Colors.white,
      fontFamily: "Product Sans",
      fontWeight: FontWeight.w400);

  static const TextStyle whiteStyle = TextStyle(
      fontSize: 70.0,
      color: Colors.white,
      fontFamily: "Product Sans",
      fontWeight: FontWeight.w200);

  static const TextStyle boldStyle = TextStyle(
    fontSize: 50.0,
    color: Colors.black,
    fontFamily: "Product Sans",
    fontWeight: FontWeight.bold,
  );

  @override
  void initState() {
    super.initState();

    pageController = PageController();
    animationController =
        AnimationController(duration: Duration(milliseconds: 750), vsync: this);

    startAnimation = CurvedAnimation(
      parent: animationController,
      curve: Interval(0.000, 0.500, curve: Curves.easeInExpo),
    );

    endAnimation = CurvedAnimation(
      parent: animationController,
      curve: Interval(0.500, 1.000, curve: Curves.easeOutExpo),
    );

    horizontalAnimation = CurvedAnimation(
      parent: animationController,
      curve: Interval(0.750, 1.000, curve: Curves.easeInOutQuad),
    );

    animationController
      ..addStatusListener((status) {
        final model = Provider.of<HomeModel>(context, listen: false);
        if (status == AnimationStatus.completed) {
          model.swapColors();
          animationController.reset();
        }
      })
      ..addListener(() {
        final model = Provider.of<HomeModel>(context, listen: false);
        if (animationController.value > 0.5) {
          model.isHalfWay = true;
        } else {
          model.isHalfWay = false;
        }
      });
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<HomeModel>(context);
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor:
          model.isHalfWay ? model.foreGroundColor : model.backGroundColor,
      body: Stack(
        children: <Widget>[
          Container(
            color:
                model.isHalfWay ? model.foreGroundColor : model.backGroundColor,
            width: screenWidth / 2.0 - Global.radius / 2.0,
            height: double.infinity,
          ),
          Transform(
            transform: Matrix4.identity()
              ..translate(
                screenWidth / 2 - Global.radius / 2.0,
                screenHeight - Global.radius - Global.bottomPadding,
              ),
            child: GestureDetector(
              onTap: () {
                if (animationController.status != AnimationStatus.forward) {
                  model.isToggled = !model.isToggled;
                  model.index++;
                  if (model.index > 3) {
                    model.index = 0;
                  }
                  pageController.animateToPage(model.index,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInOutQuad);
                  animationController.forward();
                }
              },
              child: Stack(
                children: <Widget>[
                  AnimatedCircle(
                    animation: startAnimation,
                    color: model.foreGroundColor,
                    flip: 1.0,
                    tween: Tween<double>(begin: 1.0, end: Global.radius),
                  ),
                  AnimatedCircle(
                    animation: endAnimation,
                    color: model.backGroundColor,
                    flip: -1.0,
                    horizontalTween:
                        Tween<double>(begin: 0, end: -Global.radius),
                    horizontalAnimation: horizontalAnimation,
                    tween: Tween<double>(begin: Global.radius, end: 1.0),
                  ),
                ],
              ),
            ),
          ),
          Container(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 0.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (context) => RegisterPage()));
                          },
                          child: Text("Register",
                              style: TextStyle(
                                color: model.index % 2 == 0
                                    ? Global.mediumYellow
                                    : Global.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.w600,
                              ))),
                      GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (context) => LoginPage()));
                          },
                          child: Text("Log In",
                              style: TextStyle(
                                color: model.index % 2 == 0
                                    ? Global.mediumYellow
                                    : Global.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.w600,
                              ))),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 40.0),
                      // Text(
                      //   "Yellow",
                      //   style: blackStyle,
                      // ),
                      // Text(
                      //   "Furniture",
                      //   style: boldStyle,
                      // ),
                      SizedBox(
                        height: 20.0,
                      ),
                      // Text(
                      //   "Temporibus autem aut\n"
                      //   "officiis debitis aut rerum\n"
                      //   "necessitatibus",
                      //   style: descriptionGreyStyle,
                      // ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          IgnorePointer(
            ignoring: true,
            child: PageView.builder(
              controller: pageController,
              itemCount: 4,
              itemBuilder: (context, index) {
                return Center(
                  // child: Image.asset("assets/images/chair_yellow.png"),
                  child: Container(
                    child: Center(
                      child: _diffContent(index),
                      // Text(
                      //   'Page ${index + 1}',
                      //   style: TextStyle(
                      //     color: index % 2 == 0
                      //         ? Global.mediumYellow
                      //         : Global.white,
                      //     fontSize: 30.0,
                      //     fontWeight: FontWeight.w900,
                      //   ),
                      // ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _diffContent(index) {
    index = index + 1;
    if (index == 1) {
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Yellow",
              style: blackStyle,
            ),
            Text(
              "Furniture",
              style: boldStyle,
            ),
            SizedBox(
              height: 10,
            ),
            Stack(
              children: <Widget>[
                // Positioned(
                //     right: 20,
                //     child: Image.asset(
                //       "assets/images/brush.png",
                //       width: 200,
                //     )),
                Positioned(
                    right: 40,
                    child: Image.asset(
                      "assets/images/brush.png",
                      width: 250,
                    )),
                // Image.asset(
                //   "assets/images/brush.png",
                //   width: 200,
                // ),
                // height: 20,
                Positioned(
                  // left: 60,
                  // right: -30,
                  child:
                      Image.asset("assets/images/chair_yellow.png", width: 400),
                ),
              ],
            ),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      );
    } else if (index == 2) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Find Out", style: whiteStyle),
          Text("More In..", style: whiteBoldStyle),
        ],
      );
    } else if (index == 3) {
      return Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "assets/images/demo_yellow_2.png",
                  width: 130.0,
                ),
                Image.asset("assets/images/shoe_rack_yellow.png", width: 120.0),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset("assets/images/rack_yellow.png", width: 130.0),
                Image.asset("assets/images/wood_chair_yellow.png",
                    width: 130.0),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset("assets/images/armchair.png", width: 130.0),
                Image.asset("assets/images/couch_yellow.png", width: 130.0),
              ],
            ),
          ],
        ),
      );
    } else if (index == 4) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Yellow", style: whiteStyle),
          Text("Furniture", style: whiteBoldStyle),
        ],
      );
    }
  }
}
