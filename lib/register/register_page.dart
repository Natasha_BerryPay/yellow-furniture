import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:yellow_furniture/login/login_page.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  var emailCtrl;
  var passwordCtrl;
  var rePasswordCtrl;
  bool _toogleEye = true;
  final _formKey = GlobalKey<FormState>();

  final RegExp emailRegex = new RegExp(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
            child: Column(children: [
          SizedBox(height: Platform.isAndroid ? 30 : 10),
          Padding(
            padding: EdgeInsets.all(16.0),
            // child: Hero(tag: "logo", child: Image.asset("")),
          ),
          Container(
              margin: EdgeInsets.fromLTRB(10.0, 150.0, 10.0, 0.0),
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      Row(
                        children: <Widget>[
                          SizedBox(width: 10),
                          Icon(
                            Icons.account_circle_rounded,
                            color: Colors.black87,
                            size: 20,
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter your email';
                                } else if (!emailRegex.hasMatch(value)) {
                                  return 'Please enter valid email';
                                }
                                return null;
                              },
                              controller: emailCtrl,
                              textInputAction: TextInputAction.next,
                              autofocus: true,
                              style: TextStyle(fontSize: 15),
                              decoration: InputDecoration(
                                  hintText: "Email", border: InputBorder.none),
                              // Input length limit to 11 digit
                              // inputFormatters: [LengthLimitingTextInputFormatter(11)],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          SizedBox(width: 10),
                          Icon(
                            Icons.lock,
                            color: Colors.black87,
                            size: 20,
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter your password';
                                }
                                return null;
                              },
                              controller: passwordCtrl,
                              textInputAction: TextInputAction.next,
                              autofocus: true,
                              obscureText: _toogleEye,
                              keyboardType: TextInputType.text,
                              style: TextStyle(fontSize: 15),
                              decoration: InputDecoration(
                                  hintText: "Password",
                                  border: InputBorder.none),
                              // Input length limit to 11 digit
                              // inputFormatters: [LengthLimitingTextInputFormatter(11)],
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _toogleEye = !_toogleEye;
                              });
                            },
                            icon: _toogleEye
                                ? Icon(
                                    FontAwesomeIcons.solidEyeSlash,
                                    size: 16,
                                    color: Colors.black,
                                  )
                                : Icon(
                                    FontAwesomeIcons.solidEye,
                                    size: 18,
                                    color: Colors.black,
                                  ),
                          )
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          SizedBox(width: 10),
                          Icon(
                            Icons.enhanced_encryption_rounded,
                            color: Colors.black87,
                            size: 20,
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter your re-type password';
                                }
                                return null;
                              },
                              controller: rePasswordCtrl,
                              textInputAction: TextInputAction.done,
                              autofocus: true,
                              obscureText: _toogleEye,
                              keyboardType: TextInputType.text,
                              style: TextStyle(fontSize: 15),
                              decoration: InputDecoration(
                                  hintText: "Re-type Password",
                                  border: InputBorder.none),
                              // Input length limit to 11 digit
                              // inputFormatters: [LengthLimitingTextInputFormatter(11)],
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _toogleEye = !_toogleEye;
                              });
                            },
                            icon: _toogleEye
                                ? Icon(
                                    FontAwesomeIcons.solidEyeSlash,
                                    size: 16,
                                    color: Colors.black,
                                  )
                                : Icon(
                                    FontAwesomeIcons.solidEye,
                                    size: 18,
                                    color: Colors.black,
                                  ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 50.0,
                      ),
                      _buttons(),
                    ],
                  ),
                ),
              )),
        ])),
      ),
    );
  }

  Widget _buttons() {
    return Container(
      height: 50.0,
      child: RaisedButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(
                context, CupertinoPageRoute(builder: (context) => LoginPage()));
          } else {
            print('Error');
          }
        },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Color(0xFFfdc14f), Color(0xFFffd078)],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            constraints: BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
            alignment: Alignment.center,
            child: Text(
              "Register",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  // Widget _msg(msg) {
  //   return Padding(
  //     padding: EdgeInsets.all(10.0),
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.end,
  //       children: [
  //         Expanded(
  //           child: Container(
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: [
  //                 Container(
  //                   padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
  //                   decoration: BoxDecoration(
  //                       color: Colors.white,
  //                       borderRadius: new BorderRadius.circular(10.0),
  //                       boxShadow: [
  //                         BoxShadow(
  //                             color: Colors.black12,
  //                             blurRadius: 5,
  //                             offset: Offset(0, 6))
  //                       ]),
  //                   child: Column(
  //                     mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                     children: <Widget>[
  //                       Stack(
  //                         children: [
  //                           Container(
  //                             child: SizedBox(
  //                                 height: 150.0,
  //                                 width: 150.0,
  //                                 child: FlareActor(
  //                                   "assets/icons/Success.flr",
  //                                   alignment: Alignment.center,
  //                                   fit: BoxFit.cover,
  //                                   animation: "Success",
  //                                 )),
  //                           ),
  //                           Positioned(
  //                             bottom: 0,
  //                             right: 0,
  //                             left: 0,
  //                             child: Row(
  //                               mainAxisAlignment: MainAxisAlignment.center,
  //                               children: <Widget>[
  //                                 Text(msg,
  //                                     style: TextStyle(
  //                                       fontWeight: FontWeight.bold,
  //                                       fontSize: 20.0,
  //                                     )),
  //                               ],
  //                             ),
  //                           ),
  //                           // Text("Please try again later"),
  //                         ],
  //                       ),
  //                       // Text(msg),

  //                       ///Black Line
  //                       SizedBox(height: 10),
  //                       Container(
  //                         height: 1,
  //                         color: Colors.black,
  //                       ),
  //                       SizedBox(height: 10),
  //                       Row(
  //                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                         children: <Widget>[
  //                           Text(
  //                             'Item',
  //                           ),

  //                           ///Product Name
  //                           Text('pln_page.pln_pascabayar',
  //                               style: TextStyle(fontWeight: FontWeight.bold)),
  //                         ],
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         ),

  //         ///Done Button
  //         _doneButton(),
  //       ],
  //     ),
  //   );
  // }

  // Widget _doneButton() {
  //   return FlatButton(
  //     color: Colors.red,
  //     splashColor: Colors.redAccent,
  //     shape: RoundedRectangleBorder(
  //         borderRadius: new BorderRadius.circular(24.0),
  //         side: BorderSide(color: Colors.red)),
  //     onPressed: () {
  //       // Navigator.pushReplacement(
  //       //     context, MaterialPageRoute(builder: (context) => LoginPage()));
  //     },
  //     child: Container(
  //         alignment: Alignment.center,
  //         width: MediaQuery.of(context).size.width * 0.7,
  //         padding: EdgeInsets.all(16),
  //         child: Text("Done",
  //             style: TextStyle(
  //               color: Colors.white,
  //             ))),
  //   );
  // }
}
