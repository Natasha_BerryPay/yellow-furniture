import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:yellow_furniture/api/local_auth_api.dart';
import 'package:yellow_furniture/commerce/screens/home/home_screen.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var emailCtrl;
  var passwordCtrl;
  var rePasswordCtrl;
  bool _toogleEye = true;
  final _formKey = GlobalKey<FormState>();
  final LocalAuthentication localAuth = LocalAuthentication();
  final RegExp emailRegex = new RegExp(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
            child: Column(children: [
          SizedBox(height: Platform.isAndroid ? 30 : 10),
          // Padding(
          //   padding: EdgeInsets.all(16.0),
          //   // child: Hero(tag: "logo", child: Image.asset("")),
          // ),
          Container(
              margin: EdgeInsets.fromLTRB(10.0, 150.0, 10.0, 0.0),
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text("Welcome!",
                          style: TextStyle(
                              fontFamily: "Product Sans",
                              fontWeight: FontWeight.w600,
                              fontSize: 30)),
                      Text("Let See What's Inside",
                          style: TextStyle(
                              fontFamily: "Product Sans",
                              fontWeight: FontWeight.w300,
                              fontSize: 30)),
                      SizedBox(height: 30.0),
                      Row(
                        children: <Widget>[
                          SizedBox(width: 10),
                          Icon(
                            Icons.account_circle_rounded,
                            color: Colors.black87,
                            size: 20,
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter your email';
                                } else if (!emailRegex.hasMatch(value)) {
                                  return 'Please enter valid email';
                                }
                                return null;
                              },
                              controller: emailCtrl,
                              textInputAction: TextInputAction.next,
                              autofocus: true,
                              style: TextStyle(fontSize: 15),
                              decoration: InputDecoration(
                                  hintText: "Email", border: InputBorder.none),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          SizedBox(width: 10),
                          Icon(
                            Icons.lock,
                            color: Colors.black87,
                            size: 20,
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter your password';
                                }
                                return null;
                              },
                              controller: passwordCtrl,
                              textInputAction: TextInputAction.next,
                              autofocus: true,
                              obscureText: _toogleEye,
                              keyboardType: TextInputType.text,
                              style: TextStyle(fontSize: 15),
                              decoration: InputDecoration(
                                  hintText: "Password",
                                  border: InputBorder.none),
                              // Input length limit to 11 digit
                              // inputFormatters: [LengthLimitingTextInputFormatter(11)],
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _toogleEye = !_toogleEye;
                              });
                            },
                            icon: _toogleEye
                                ? Icon(
                                    FontAwesomeIcons.solidEyeSlash,
                                    size: 16,
                                    color: Colors.black,
                                  )
                                : Icon(
                                    FontAwesomeIcons.solidEye,
                                    size: 18,
                                    color: Colors.black,
                                  ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 40.0,
                      ),
                      _buttons(),
                      // buildAvailability(context),
                      SizedBox(height: 24),
                      Text("Or"),
                      SizedBox(height: 24),
                      buildAuthenticate(context),
                    ],
                  ),
                ),
              )),
        ])),
      ),
    );
  }

  Widget _buttons() {
    return Container(
      height: 50.0,
      child: RaisedButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Navigator.push(context,
                CupertinoPageRoute(builder: (context) => HomeScreen()));
          } else {
            print('Error');
          }
        },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Color(0xFFfdc14f), Color(0xFFffd078)],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            constraints: BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
            alignment: Alignment.center,
            child: Text(
              "Log In",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildText(String text, bool checked) => Container(
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            checked
                ? Icon(Icons.check, color: Colors.green, size: 24)
                : Icon(Icons.close, color: Colors.red, size: 24),
            const SizedBox(width: 12),
            Text(text, style: TextStyle(fontSize: 24)),
          ],
        ),
      );

  Widget buildAuthenticate(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Log in using "),
        GestureDetector(
          onTap: () async {
            final isAuthenticated = await LocalAuthApi.authenticate();

            if (isAuthenticated) {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreen()),
              );
            }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                "Fingerprint",
                style: TextStyle(
                  color: Colors.blue,
                ),

                // textAlign: TextAlign.center,
              ),
              Icon(
                Icons.fingerprint,
                size: 30.0,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
